from django.apps import AppConfig


class ProfilPribadiConfig(AppConfig):
    name = 'profil_pribadi'
