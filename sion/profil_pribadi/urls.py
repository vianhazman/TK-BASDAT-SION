from django.conf.urls import url
from .views import index, index_relawan, index_donatur, index_sponsor
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^profil_relawan', index_relawan, name='index_relawan'),
    url(r'^profil_donatur', index_donatur, name='index_donatur'),
    url(r'^profil_sponsor', index_sponsor, name='index_sponsor'),
]
