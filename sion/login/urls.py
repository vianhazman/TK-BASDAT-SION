from django.conf.urls import url
from .views import *
urlpatterns = [
    url(r'^$', show_login ,name='login'),
    url(r'^login/', login, name='log'),
    url(r'^logout/', logout, name='logout'),
    url(r'^register/', show_register ,name='register'),
    url(r'^get_city/', get_city ,name='get_city'),
    url(r'^register_sponsor/', register_sponsor ,name='register_sponsor'),
    url(r'^register_donatur/', register_donatur ,name='register_donatur'),
    url(r'^register_relawan/', register_relawan ,name='register_relawan'),
    url(r'^donatur/', show_index_donatur ,name='donatur'),
    url(r'^relawan/', show_index_relawan ,name='relawan'),
    url(r'^sponsor/', show_index_sponsor ,name='sponsor'),
    url(r'^pengurus/', show_index_pengurus ,name='pengurus')
    ]