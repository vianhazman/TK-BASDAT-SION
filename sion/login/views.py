from django.shortcuts import render
from django.db import connection
import http.client
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

response = {}
def show_login(request):
        if ("role" in request.session):
                return HttpResponseRedirect(request.session["role"].lower()+'/')
        return render(request, 'login.html', response)

@csrf_exempt
def login(request):
	 if(request.method == 'POST'):
                email = request.POST['user']
                password = request.POST['pass']
                cursor=connection.cursor()
                cursor.execute("SELECT * from SION.USERS where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if (select):
                        cursor.execute("SELECT password from SION.USERS where EMAIL='"+email+"'")
                        select=cursor.fetchone()
                        if select[0] == password:
                                request.session["user"]=email
                                cursor.execute("SELECT nama from SION.USERS where EMAIL='"+email+"'")
                                select=cursor.fetchone()
                                request.session["nama"]=select[0]
                                cursor.execute("SELECT * from SION.RELAWAN where EMAIL='"+email+"'")
                                select=cursor.fetchone()
                                if (select):
                                        request.session["role"]="relawan"
                                        return HttpResponseRedirect('/relawan/')
                                cursor.execute("SELECT * from SION.DONATUR where EMAIL='"+email+"'")
                                select=cursor.fetchone()
                                if (select):
                                        request.session["role"]="donatur"
                                        return HttpResponseRedirect('/donatur/')
                                cursor.execute("SELECT * from SION.SPONSOR where EMAIL='"+email+"'")
                                select=cursor.fetchone()
                                if (select):
                                        request.session["role"]="sponsor"
                                        return HttpResponseRedirect('/sponsor/')
                                cursor.execute("SELECT * from SION.PENGURUS_ORGANISASI where EMAIL='"+email+"'")
                                select=cursor.fetchone()
                                if (select):
                                        request.session["role"]="pengurus"
                                        return HttpResponseRedirect('/pengurus/')

                                return HttpResponse(request.session["role"])
                        else:
                                return HttpResponse("Password Salah")
                return HttpResponse("Not Found!")

@csrf_exempt
def register_sponsor(request):
        if(request.method == 'POST'):
                name=request.POST["name"]
                email=request.POST["email"]
                pwd = request.POST["pass"]
                address = request.POST["alamat"]
                print("Alamat: "+ address)
                logo = request.POST["docfile"]
                cursor=connection.cursor()
                cursor.execute("SELECT * from SION.USERS where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if (select):
                        return HttpResponse("Email sudah terdaftar")  
                cursor.execute("INSERT INTO SION.USERS (email,password,nama,alamat_lengkap) VALUES('"+email+"','"+pwd+"','"+name+"','"+address+"')")
                cursor.execute("INSERT INTO SION.SPONSOR (email,logo_sponsor) VALUES('"+email+"','"+logo+"')")
                request.session["user"]=email
                request.session["nama"]=name
                request.session["role"]="sponsor"
                return HttpResponseRedirect('/sponsor/')

@csrf_exempt
def register_donatur(request):
        if(request.method == 'POST'):
                name=request.POST["name"]
                email=request.POST["email"]
                pwd = request.POST["pass"]
                address = request.POST["alamat"]
                print("Alamat: "+ address)
                cursor=connection.cursor()
                cursor.execute("SELECT * from SION.USERS where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if (select):
                        return HttpResponse("Email sudah terdaftar")  
                cursor.execute("INSERT INTO SION.USERS (email,password,nama,alamat_lengkap) VALUES('"+email+"','"+pwd+"','"+name+"','"+address+"')")
                cursor.execute("INSERT INTO SION.DONATUR (email,saldo) VALUES('"+email+"','0')")
                request.session["user"]=email
                request.session["nama"]=name
                request.session["role"]="donatur"
                return HttpResponseRedirect('/donatur/')

@csrf_exempt
def register_relawan(request):
        if(request.method == 'POST'):
                name=request.POST["name"]
                email=request.POST["email"]
                pwd = request.POST["pass"]
                birth=request.POST["birth"]
                address = request.POST["alamat"]
                print("Alamat: "+ address)
                nomor = request.POST["number"]
                keahlian = request.POST["skills"]
                cursor=connection.cursor()
                cursor.execute("SELECT * from SION.USERS where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if (select):
                        return HttpResponse("Email sudah terdaftar")  
                cursor.execute("INSERT INTO SION.USERS (email,password,nama,alamat_lengkap) VALUES('"+email+"','"+pwd+"','"+name+"','"+address+"')")
                cursor.execute("INSERT INTO SION.RELAWAN (email, no_hp, tanggal_lahir) VALUES('"+email+"','"+nomor+"','"+birth+"')")
                keahlian = keahlian.split(",")
                for i in keahlian:
                        cursor.execute("INSERT INTO SION.KEAHLIAN_RELAWAN (email, keahlian)  VALUES('"+email+"','"+i+"')")
                request.session["user"]=email
                request.session["nama"]=name
                request.session["role"]="relawan"
                return HttpResponseRedirect('/relawan/')


def get_city(request):

    conn = http.client.HTTPSConnection("api.rajaongkir.com")

    headers = { 'key': "ea827133edd06f4d89a5296c0661c3e4" }

    conn.request("GET", "/starter/city", headers=headers)
    res = conn.getresponse()
    data = res.read()
    data = data.decode("utf-8")
    data_city = json.loads(data)
    # print(data)
    return JsonResponse(data_city)

def show_register(request):
	return render(request, 'register.html', response)

def logout(request):
        request.session.clear()
        return render(request, 'login.html', response)

# TO-DO: ini masih hardcode, nanti otomatis pake abis ngebaca rolenya
def show_index_donatur(request):
	return render(request, 'index_donatur.html', response)

def show_index_relawan(request):
        response={'nama':request.session["nama"]}
        return render(request, 'index_relawan.html', response)

def show_index_sponsor(request):
        response={'nama':request.session["nama"]}
        return render(request, 'index_sponsor.html', response)

def show_index_pengurus(request):
        response={'nama':request.session["nama"]}
        return render(request, 'index_pengurus.html', response)

